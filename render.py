import logging
import os

import jinja2
from jinja2 import FileSystemLoader

import config

jenv = jinja2.Environment(loader=FileSystemLoader("templates/"))


def render_overview():
    files = [x for x in os.listdir(config.PUBLIC_PATH) if x.endswith(".rsc")]
    overview_filename = os.path.join(config.PUBLIC_PATH, "index.html")
    template = jenv.get_template("index.jinja2")
    content = template.render(lists=files)
    with open(overview_filename, "w", encoding="utf-8") as fp:
        fp.write(content)
    logging.info(f"Created {overview_filename}")


REPOS = [
    {"url": "https://github.com/herrbischoff/country-ip-blocks"}
]

BLOCKLIST_URL = "https://raw.githubusercontent.com/firehol/blocklist-ipsets/master/firehol_level1.netset"

WORKDIR_PATH = "workdir"

PUBLIC_PATH = "public"

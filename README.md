# MikroTik GeoIP

 - Built weekly
 - GeoIP Source: https://github.com/herrbischoff/country-ip-blocks
 - Blocklist Source: https://github.com/firehol/blocklist-ipsets

## Available lists
https://mikrotikgeoip-muqiuq-9d674e92c7d1b98594a57d5312a8dd06e6c6e7ff35.gitlab.io/

## GeoIP - Example for CH
```mikrotik
/ip firewall address-list remove [/ip firewall address-list find list="GeoIP-ch"]
/tool/fetch url="https://mikrotikgeoip-muqiuq-9d674e92c7d1b98594a57d5312a8dd06e6c6e7ff35.gitlab.io/ch.rsc" output=file dst-path=GeoIP.rsc
/import file-name=GeoIP.rsc
```

## GeoIP - Example for US
```mikrotik
/ip firewall address-list remove [/ip firewall address-list find list="GeoIP-us"]
/tool/fetch url="https://mikrotikgeoip-muqiuq-9d674e92c7d1b98594a57d5312a8dd06e6c6e7ff35.gitlab.io/us.rsc" output=file dst-path=GeoIP.rsc
/import file-name=GeoIP.rsc
```

## Blocklist - Firehol
```mikrotik
/ip firewall address-list remove [/ip firewall address-list find list="Blocklist"]
/tool/fetch url="https://mikrotikgeoip-muqiuq-9d674e92c7d1b98594a57d5312a8dd06e6c6e7ff35.gitlab.io/blocklist.rsc" output=file dst-path=Blocklist.rsc
/import file-name=Blocklist.rsc
```



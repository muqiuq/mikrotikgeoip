import logging
import os.path

import requests

import config
import render
import repocollector

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT, force=True)

repocollector.create_or_update_repos()

ipv4path = os.path.join(config.WORKDIR_PATH, "country-ip-blocks", "ipv4")

ipv4lists = os.listdir(ipv4path)

if not os.path.exists(config.PUBLIC_PATH):
    os.mkdir(config.PUBLIC_PATH)


def build_geoip():
    for ipv4list in ipv4lists:
        countryCode = ipv4list.split(".")[0]
        with open(os.path.join(ipv4path, ipv4list), "r") as infile:
            with open(os.path.join(config.PUBLIC_PATH, f"{countryCode}.rsc"), "w") as outfile:
                while True:
                    line = infile.readline()
                    if line == '':
                        break
                    oneIpv4 = line.strip()
                    if oneIpv4 != "" and not oneIpv4.startswith("#"):
                        outfile.write(f"/ip/firewall/address-list/add list=GeoIP-{countryCode} address={oneIpv4}\n")


def build_firehol():
    req = requests.get(config.BLOCKLIST_URL)
    lines = req.content.decode("utf-8").split("\n")
    with open(os.path.join(config.PUBLIC_PATH, f"blocklist.rsc"), "w") as outfile:
        for line in lines:
            oneIpv4 = line.strip()
            if oneIpv4 != "" and not oneIpv4.startswith("#"):
                outfile.write(f"/ip/firewall/address-list/add list=Blocklist address={oneIpv4}\n")


build_geoip()
build_firehol()

render.render_overview()

logging.info("Finished")